package hu.elte.debtManager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elte.debtManager.entities.Group;
import hu.elte.debtManager.entities.User;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
class GroupControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private String jsonToString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetAllGroups() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/groups"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetGroupById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/groups/1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailToGetGroupById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/groups/99"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailDeleteGroupByIdBecauseNotEmpty() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/groups/2"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailDeleteGroupById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/groups/99"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldCreateNewGroup() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/groups")
                .content(jsonToString(
                        Group.builder()
                                .name("Sitabor")
                                .build()
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Sitabor"));

    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldUpdateGroup() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/groups/1")
                .content(jsonToString(
                        Group.builder()
                                .name("ELTE-Tabor")
                                .build()
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("ELTE-Tabor"));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldFailToUpdateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/groups/99"))
                .andExpect(status().isBadRequest());
    }
}