package hu.elte.debtManager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elte.debtManager.entities.GroupMember;
import hu.elte.debtManager.entities.User;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
public class GroupMemberControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private String jsonToString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetUserGrupsByUserId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
        .get("/debts/1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailGetUserGrupsByUserId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/debts/99"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetUsersOfGroupByGroupId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/debts/group/1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailGetUsersOfGroupByGroupId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/debts/group/999"))
                .andExpect(status().isNotFound());
    }



    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetDebtOfUserByGroup()throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/debts/1/2"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());

    }


    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailGetDebtOfUserByGorup()throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/debts/77/77"))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldDeleteUserFromGroup() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/debts/1/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailDeleteUserFromGroup() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/debts/99/99"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldCreateGroupMember() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/debts/2/2/999")
                .content(jsonToString(
                 ""
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailCreateGroupMember() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/debts/999/999/1")
                .content(jsonToString(
                        ""
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldUpdateGroupMemberDebt() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/debts/1/2/999")
                .content(jsonToString(
                        ""
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailUpdateGroupMemberDebt() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/debts/999/999/1")
                .content(jsonToString(
                        ""
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }
}
