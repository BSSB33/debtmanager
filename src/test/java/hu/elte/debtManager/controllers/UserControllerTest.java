package hu.elte.debtManager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.elte.debtManager.entities.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
//@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private String jsonToString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetAllUsers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users/1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailToGetUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users/9999"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetUserByName() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users/getByName/Laura"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailGetUserByName() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users/getByName/cicamica"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldDeleteUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/2"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldFailDeleteUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/9999"))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldCreateNewUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .content(jsonToString(
                        User.builder()
                        .username("cica")
                        .password("mica")
                        .email("ci@ca.com")
                        .role(User.Role.USER)
                        .build()
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }



    @Test
    @WithMockUser(roles = "USER")
    public void shouldUpdateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/users/1")
                .content(jsonToString(
                        User.builder()
                        .username("GergoButUpdated")
                        .password("password")
                        .role(User.Role.USER)
                        .email("gergo@email.com")
                        .build()
                ))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("GergoButUpdated"));
    }


    @Test
    @WithMockUser(roles = "USER")
    public void shouldFailToUpdateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/users/9999"))
                .andExpect(status().isBadRequest());
    }


}
