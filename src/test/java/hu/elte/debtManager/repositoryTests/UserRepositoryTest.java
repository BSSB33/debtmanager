package hu.elte.debtManager.repositoryTests;

import hu.elte.debtManager.entities.User;
import hu.elte.debtManager.repositories.UserRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    User testUser = new User("TestUser", "test@email.com", "testpwd", null, User.Role.USER);

    @Test
    public void userRepositoryIntegrationTest() {
        userRepository.save(testUser);
        Optional<User> foundTestUserName = userRepository.findByUsername("TestUser");

        assertEquals(foundTestUserName.getClass(), userRepository.findByUsername("TestUser").getClass());
        assertTrue(foundTestUserName.isPresent());
        assertEquals("test@email.com", testUser.getEmail());
    }
}