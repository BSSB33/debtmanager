package hu.elte.debtManager.repositoryTests;

import hu.elte.debtManager.entities.Group;
import hu.elte.debtManager.repositories.GroupRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class GroupRepositoryTest {
    @Autowired
    private GroupRepository groupRepository;

    Group testGroup = new Group("testGroup");

    @Test
    public void groupRepositoryIntegrationTest() {
        groupRepository.save(testGroup);
        Optional<Group> foundedTestGroupName = groupRepository.findByName("testGroup");

        assertEquals(foundedTestGroupName.getClass(), groupRepository.findById(1L).getClass());
        assertTrue(foundedTestGroupName.isPresent());
        assertEquals("testGroup", foundedTestGroupName.get().getName() );
    }
}
