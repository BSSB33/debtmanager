package hu.elte.debtManager.repositoryTests;

import hu.elte.debtManager.entities.Group;
import hu.elte.debtManager.entities.GroupMember;
import hu.elte.debtManager.entities.User;
import hu.elte.debtManager.repositories.GroupMemberRepository;
import hu.elte.debtManager.repositories.GroupRepository;
import hu.elte.debtManager.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class GroupMemberRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupMemberRepository  groupMemberRepository;

    Group testGroup = new Group("testGroup");
    User testUser = new User("TestUser", "test@email.com", "testpwd", null, User.Role.USER);


    @Test
    public void groupMemberRepositoryIntegrationTest() {
        groupRepository.save(testGroup);
        userRepository.save(testUser);
        GroupMember groupMember = new GroupMember(testUser, 1500d, testGroup);
        groupMemberRepository.save(groupMember);
        List<GroupMember> foundedNewGroupMember = groupMemberRepository.findByUserId(testUser.getId());

        assertFalse(foundedNewGroupMember.isEmpty());

        groupMemberRepository.deleteAll();
        List<GroupMember> groupMemberList = groupMemberRepository.findAll();
        assertTrue(groupMemberList.isEmpty());
        System.out.println("********************* NICE TESTS *********************");
    }
}
