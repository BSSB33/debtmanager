INSERT INTO USER_TABLE(email, password, role, username) VALUES('gergo@email.com','$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', 'ADMIN', 'Gergo');
INSERT INTO USER_TABLE(email, password, role, username) VALUES('laura@email.com','$2a$04$YDiv9c./ytEGZQopFfExoOgGlJL6/o0er0K.hiGb5TGKHUL8Ebn..', 'USER', 'Laura');

INSERT INTO GROUP_TABLE(name) VALUES ('ELTE');
INSERT INTO GROUP_TABLE(name) VALUES ('Kis csoport');

INSERT INTO GROUP_MEMBER_TABLE(debt, group_id, user_id) VALUES (130, 1, 1);
INSERT INTO GROUP_MEMBER_TABLE(debt, group_id, user_id) VALUES (200, 1, 2);
INSERT INTO GROUP_MEMBER_TABLE(debt, group_id, user_id) VALUES (-100, 2, 1);