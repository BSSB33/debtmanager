package hu.elte.debtManager.controllers;

import hu.elte.debtManager.entities.Group;
import hu.elte.debtManager.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/groups")
public class GroupController {
    @Autowired
    private GroupRepository groupRepository;

    @GetMapping("")
    public ResponseEntity<Iterable<Group>> getAll() {
        return new ResponseEntity<>(groupRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Group> getById(@PathVariable Long id) {
        Optional<Group> group = groupRepository.findById(id);
        if (group.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(group.get());
    }

    @GetMapping("/getByName/{groupName}")
    public ResponseEntity<Group> getByGroupName(@PathVariable String groupName) {
        Optional<Group> group = groupRepository.findByName(groupName);
        if (group.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(group.get());
    }


    //TODO: csak akkor deleteli ha nincs benne user
    @DeleteMapping("/{groupId}")
    public ResponseEntity deleteById(@PathVariable Long groupId) {
        Optional<Group> group = groupRepository.findById(groupId);
        if (!group.isPresent()) {
           return ResponseEntity.notFound().build();
        }else if(group.get().getGroupMembers().isEmpty()) {
              groupRepository.deleteById(groupId);
        }else {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("")
    public ResponseEntity<Group> newGroup(@RequestBody Group group) {
        return ResponseEntity.ok(groupRepository.save(group));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Group> updateGroup(@PathVariable Long id, @RequestBody Group newGroup) {
        Optional<Group> oldGroup = groupRepository.findById(id);
        if (oldGroup.isPresent()) {
            groupRepository.delete(oldGroup.get());

            return ResponseEntity.ok(groupRepository.save(newGroup));
        }
        return ResponseEntity.notFound().build();
    }
}
