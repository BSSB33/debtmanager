package hu.elte.debtManager.controllers;

import hu.elte.debtManager.entities.Group;
import hu.elte.debtManager.entities.GroupMember;
import hu.elte.debtManager.entities.User;
import hu.elte.debtManager.repositories.GroupMemberRepository;
import hu.elte.debtManager.repositories.GroupRepository;
import hu.elte.debtManager.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/debts")
public class GroupMemberController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @GetMapping()
    public ResponseEntity<List<GroupMember>> allGroups() {
        return new ResponseEntity<>(groupMemberRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<Group>> getGroupsOfUserByUserId(@PathVariable Long userId){
        List<GroupMember> groupMembersByUserId = groupMemberRepository.findByUserId(userId);
        if(groupMembersByUserId.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<Group> groupsOfUser = new ArrayList<>();
        for(GroupMember member: groupMembersByUserId){
            groupsOfUser.add(member.getGroup());
        }
        return ResponseEntity.ok(groupsOfUser);
    }


    @GetMapping("/group/{groupId}")
    public ResponseEntity<List<User>> getMembersOfGroupByGroupId(@PathVariable Long groupId){
        List<GroupMember> groupMembersByGroupId = groupMemberRepository.findByGroupId(groupId);
        if(groupMembersByGroupId.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        List<User> usersOfGroup = new ArrayList<>();
        for(GroupMember member : groupMembersByGroupId){
            usersOfGroup.add(member.getUser());
        }
        return ResponseEntity.ok(usersOfGroup);
    }


    @GetMapping("/{groupId}/{userId}")
    public ResponseEntity<Double> getDebtOfUserByGroup(@PathVariable Long groupId, @PathVariable Long userId) {
        List<GroupMember> groupMembersByUserId = groupMemberRepository.findByUserId(userId);
        if(groupMembersByUserId.isEmpty())
        {
            return  ResponseEntity.notFound().build();
        }
        for(GroupMember member : groupMembersByUserId)
        {
            if(member.getGroup().getId().equals(groupId)){
                return ResponseEntity.ok(member.getDebt());
            }
        }
        return ResponseEntity.notFound().build();
    }


    @DeleteMapping("/{groupId}/{userId}")
    public ResponseEntity deleteUserFromGroup(@PathVariable Long groupId, @PathVariable Long userId){
        List<GroupMember> groupMembersByGroupId = groupMemberRepository.findByGroupId(groupId);
        if(groupMembersByGroupId.isEmpty())
        {
            return  ResponseEntity.notFound().build();
        }
        for(GroupMember member : groupMembersByGroupId)
        {
            if(member.getUser().getId().equals(userId))
            {
                groupMemberRepository.deleteById(member.getId());
                return ResponseEntity.ok().build();
            }
        }
        return ResponseEntity.notFound().build();
    }



    @PostMapping("/{userId}/{groupId}/{debt}")
    public ResponseEntity<GroupMember> newUser(@PathVariable Long userId, @PathVariable Long groupId, @PathVariable Double debt) {
        Optional<User> user = userRepository.findById(userId);
        Optional<Group> group = groupRepository.findById(groupId);
        if(user.isPresent() && group.isPresent())
        {
            GroupMember newMember = new GroupMember(user.get() ,debt, group.get());
            return ResponseEntity.ok(groupMemberRepository.save(newMember));
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{groupId}/{userId}/{debt}")
    public ResponseEntity updateUserDebtByGroupIdAndUserId(@PathVariable Long groupId, @PathVariable Long userId, @PathVariable Double debt)
    {
        List<GroupMember> groupMembersByGroupId = groupMemberRepository.findByGroupId(groupId);
        if(groupMembersByGroupId.isEmpty())
        {
            return  ResponseEntity.notFound().build();
        }
        for(GroupMember member : groupMembersByGroupId)
        {
            if(member.getUser().getId() == userId)
            {
                member.setDebt(debt);

                return ResponseEntity.ok(groupMemberRepository.save(member));
            }
        }
        return ResponseEntity.notFound().build();
    }
    
}
