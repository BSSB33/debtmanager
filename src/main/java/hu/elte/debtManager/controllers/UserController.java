package hu.elte.debtManager.controllers;

import hu.elte.debtManager.entities.User;
import hu.elte.debtManager.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @GetMapping("")
    public ResponseEntity<Iterable<User>> getAll() {
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getById(@PathVariable Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user.get());
    }


    @GetMapping("/getByName/{userName}")
    public ResponseEntity<User> getByName(@PathVariable String userName) {
        Optional<User> user = userRepository.findByUsername(userName);
        if (!user.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteById(@PathVariable Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
           return ResponseEntity.notFound().build();
        }
        userRepository.deleteById(id);

        return ResponseEntity.ok().build();
    }



    @PostMapping("")
    public ResponseEntity<User> newUser(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return ResponseEntity.ok(userRepository.save(user));
    }


   @PutMapping("/{id}")
   public ResponseEntity updateUser(@PathVariable Long id, @RequestBody User newUser) {
       Optional<User> oldUser = userRepository.findById(id);
       if (oldUser.isPresent()) {
           oldUser.get().setEmail(newUser.getEmail());
           oldUser.get().setUsername(newUser.getUsername());
           String password= passwordEncoder.encode(newUser.getPassword());
           oldUser.get().setPassword(password);
           return ResponseEntity.ok(userRepository.save(oldUser.get()));
       }
       return ResponseEntity.notFound().build();
   }

}