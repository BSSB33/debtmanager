package hu.elte.debtManager.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Groups contains GroupMembers which are the essential part of the system.
 */
@Table(name = "GROUP_TABLE")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<GroupMember> groupMembers;

    /**
     * Constructs an empty Group
     * @param name Name of the new Group
     */
    public Group(String name) {
        this.name = name;
    }
}
