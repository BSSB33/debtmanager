package hu.elte.debtManager.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Basic Users with roles.
 * Users can be assigned to Groups, and can have debt at multiple groups at the same time.
 */
@Table(name = "USER_TABLE")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<GroupMember> groups;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    /**
     * Generates a User and adds it to a group
     * @param userName Username of user
     * @param email Email address of user
     * @param password Password of user
     * @param groups The groups the user should be added
     * @param role The Role of user
     */
    public User(String userName, String email, String password, List<GroupMember> groups, Role role) {
        this.username = userName;
        this.email = email;
        this.password = password;
        this.groups = groups;
        this.role = role;
    }

    /**
     * Generates a User
     * @param userName Username of user
     * @param email Email address of user
     * @param password Password of user
     * @param role The Role of user
     */
    public User(String userName, String email, String password, Role role) {
        this.username = userName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    /**
     * User roles
     */
    public enum Role {
        USER, ADMIN
    }
}