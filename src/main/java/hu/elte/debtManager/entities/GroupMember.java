package hu.elte.debtManager.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Group Members are users with debt, assigned to Groups
 */
@Table(name = "GROUP_MEMBER_TABLE")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GroupMember {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn
    private User user;

    /**
     * Can be Positive and Negative
     */
    private Double debt;

    @ManyToOne
    @JoinColumn
    private Group group;

    /**
     * Creates/Assign a New GroupMember
     * @param user The user to assign
     * @param debt The starting Debt of User.
     * @param group The Group to assign the User to.
     */
    public GroupMember(User user, Double debt, Group group) {
        this.user = user;
        this.debt = debt;
        this.group = group;
    }

}
