package hu.elte.debtManager.repositories;

import hu.elte.debtManager.entities.GroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface GroupMemberRepository extends JpaRepository<GroupMember, Long>{
    /**
     * Returns GroupMembers by User Id
     * @param id User ID
     * @return List of GroupMembers
     */
    List<GroupMember> findByUserId(Long id);

    /**
     * Returns GroupMembers by Group Id
     * @param id Group ID
     * @return List of GroupMembers
     */
    List<GroupMember> findByGroupId(Long id);

    /**
     * Returns GroupMembers by Debt amount
     * Used to find Users in "trouble"
     * @param debt Debt amount
     * @return List of GroupMembers
     */
    List<GroupMember> findByDebt(Double debt);
}
