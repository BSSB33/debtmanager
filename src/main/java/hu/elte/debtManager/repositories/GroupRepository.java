package hu.elte.debtManager.repositories;

import hu.elte.debtManager.entities.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

    /**
     * Returns a Group by ID
     * @param id The ID of group
     * @return List of Groups
     */
    @Override
    Optional<Group> findById(Long id);

    /**
     * Returns a Group by Group name
     * @param name The name of Group to find
     * @return List of Groups
     */
    Optional<Group> findByName(String name);
}
