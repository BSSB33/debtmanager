package hu.elte.debtManager.repositories;

import hu.elte.debtManager.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Returns a User by ID
     * @param id The ID of User to find
     * @return Optional User (User/null)
     */
    @Override
    Optional<User> findById(Long id);

    /**
     * Returns a User by username
     * @param username The username of User to find
     * @return Optional User (User/null)
     */
    Optional<User> findByUsername(String username);

    /**
     * Returns a User by email
     * @param email The email of User to find
     * @return Optional User (User/null)
     */
    Optional<User> findByEmail(String email);
}
